import os

TORNADO_PORT = os.environ.get('TORNADO_PORT') or 8000
RDB_HOST =  os.environ.get('RDB_HOST') or 'rethinkdb_1'
RDB_PORT = os.environ.get('RDB_PORT') or 28015
PROJECT_DB = 'userfeed'
PROJECT_TABLE = 'users'
