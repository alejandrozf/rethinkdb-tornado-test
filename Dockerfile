FROM python:3.9

ENV PYTHONUNBUFFERED 1
RUN mkdir -p /opt/services/webapp/src
#VOLUME ["/opt/services/webapp/src"]
# We copy the requirements.txt file first to avoid cache invalidations
COPY requirements/base.txt /opt/services/webapp/src/
COPY requirements/local.txt /opt/services/webapp/src/
WORKDIR /opt/services/webapp/src
RUN pip install -r base.txt
RUN pip install -r local.txt
COPY . /opt/services/webapp/src
