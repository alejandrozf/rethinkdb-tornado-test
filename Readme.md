# To run
---------------
1) To install:

- docker-compose build

2) To run:

- docker-compose up -d

3) To use

- Go to http://localhost:8000 and login any user
- Open an incognito window and go also to http://localhost:8000 and login any other data
- Observe that notification of the logged user on incognito windows appears on the first window
- You can also go to  http://localhost:8080 to inspect RethinkDB

4) To add another machine with RethinkDB using docker (for sharding and replication)
- docker run -d --name rethinkdb_2 --network rethinkdb_test_webapp_network rethinkdb bash -c "rethinkdb --join rethinkdb_1:29015 --bind all"
- Also read https://rethinkdb.com/docs/start-a-server/#a-rethinkdb-cluster-using-multiple-machines